package com.githubjavapop.model

data class Repository(val name: String,
                      val description: String,
                      val forks_count: Int,
                      val stargazers_count: Int,
                      val owner: User?)


data class PullRequest(val owner: User,
                       val title: String,
                       val body: String,
                       val created_at: String,
                       val html_url: String)