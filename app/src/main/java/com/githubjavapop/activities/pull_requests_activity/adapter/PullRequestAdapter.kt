package com.githubjavapop.activities.pull_requests_activity.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.githubjavapop.R
import com.githubjavapop.activities.pull_requests_activity.view_holder.PullRequestViewHolder
import com.githubjavapop.model.PullRequest

class PullRequestAdapter(context: OnPullClickListener, internal var pullRequestList: List<PullRequest>)
    : RecyclerView.Adapter<PullRequestViewHolder>(){

    private var mListener: OnPullClickListener? = null

    init {
        mListener = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PullRequestViewHolder {
        return PullRequestViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.row_pull_requests, parent, false))
    }

    override fun getItemCount(): Int {
        return pullRequestList.size
    }

    override fun onBindViewHolder(holder: PullRequestViewHolder, position: Int) {

        val pullRequest = pullRequestList.get(position)

        holder.mTvBodyPullRequest.setText(pullRequest.body)
        holder.mTvTitlePullRequest.setText(pullRequest.title)
        holder.mTvUsername.setText(pullRequest.owner.login)
        holder.setImageUser(pullRequest.owner.avatar_url)
        holder.setDate(pullRequest.created_at)

        holder.itemView.setOnClickListener { v ->
            mListener?.onPulClick(pullRequest.html_url)
        }

    }

    interface OnPullClickListener {
        fun onPulClick(html_url: String)
    }

}