package com.githubjavapop.activities.pull_requests_activity.view_holder

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.row_pull_requests.view.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.githubjavapop.R


class PullRequestViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
    val mUserImage = itemView.circle_user_image
    val mTvUsername = itemView.tv_username
    val mTvDate = itemView.tv_date
    val mTvTitlePullRequest = itemView.tv_title_pull_request
    val mTvBodyPullRequest = itemView.tv_body_pull_request

    fun setImageUser(url: String?) {

        val options = RequestOptions()
                .centerCrop()
                .circleCrop()
                .placeholder(R.drawable.ic_user_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.ic_user_placeholder)

        Glide.with(itemView.context)
                .load(url)
                .apply(options)
                .into(mUserImage)
    }


    fun setDate(date: String?){

        val newDate = date?.replace("T", " ")
                ?.replace("Z", "")
        mTvDate.setText(newDate)
    }


}
