package com.githubjavapop.activities.pull_requests_activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.githubjavapop.R
import com.githubjavapop.activities.pull_requests_activity.adapter.PullRequestAdapter
import com.githubjavapop.api.GitHubAPI
import com.githubjavapop.model.PullRequest
import com.githubjavapop.util.INTENT_REPOSITORY_ID
import com.githubjavapop.util.INTENT_USER_ID
import kotlinx.android.synthetic.main.activity_repository.*
import kotlinx.android.synthetic.main.toolbar_pull_request_activity.*
import org.jetbrains.anko.browse
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import android.view.View


class PullRequestsActivity : AppCompatActivity(), PullRequestAdapter.OnPullClickListener {

    private var pullRequestList = mutableListOf<PullRequest>()
    private var repositoryId: String? = null
    private var userId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repository)

        userId = intent.getStringExtra(INTENT_USER_ID)
                ?: throw IllegalStateException("field $INTENT_USER_ID não foi encontrada na intent.")

        repositoryId = intent.getStringExtra(INTENT_REPOSITORY_ID)
                ?: throw IllegalStateException("field $INTENT_REPOSITORY_ID não foi encontrada na intent.")

        initViews()

        initAPI()

    }

    private fun initViews(){

        //toolbar
        setSupportActionBar(toolbar_pull_request_activity)
        supportActionBar?.setTitle(repositoryId)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        //recycler view
        rv_pull_requests.setHasFixedSize(true)
        rv_pull_requests.layoutManager = LinearLayoutManager(this)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.getItemId()
        if (id == android.R.id.home){
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun initAPI(){

        val api = GitHubAPI()
        api.loadPullRequests(userId, repositoryId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    pullRequest -> pullRequestList.add(pullRequest)
                }, {
                    e -> e.printStackTrace()
                }, {
                    displayRepositories(pullRequestList)
                })

    }

    private fun displayRepositories(pullRequests: List<PullRequest>?){

        val adapter = PullRequestAdapter(this, pullRequests!!)
        rv_pull_requests.adapter = adapter
        progress.setVisibility(View.GONE)
    }

    override fun onPulClick(html_url: String) {
        browse(html_url)
    }

}
