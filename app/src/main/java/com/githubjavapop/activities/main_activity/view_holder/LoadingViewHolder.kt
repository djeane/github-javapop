package com.githubjavapop.activities.main_activity.view_holder

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.row_progress.view.*


class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
    val mProgress = itemView.progress
}

