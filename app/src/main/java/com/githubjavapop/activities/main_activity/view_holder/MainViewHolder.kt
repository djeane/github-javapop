package com.githubjavapop.activities.main_activity.view_holder

import android.support.v7.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.githubjavapop.R
import kotlinx.android.synthetic.main.row_repositories.view.*


class MainViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

    val mUserImage = itemView.circle_user_image
    val mTvUsername = itemView.tv_username
    val mTvRepositoryName = itemView.tv_repository_name
    val mTvDescription = itemView.tv_description
    val mTvForksNumber = itemView.tv_forks_number
    val mTvStarsNumber = itemView.tv_stars_number

    fun setImageUser(url: String?) {

        val options = RequestOptions()
                .centerCrop()
                .circleCrop()
                .placeholder(R.drawable.ic_user_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.ic_user_placeholder)

        Glide.with(itemView.context)
                .load(url)
                .apply(options)
                .into(mUserImage)

    }

}

