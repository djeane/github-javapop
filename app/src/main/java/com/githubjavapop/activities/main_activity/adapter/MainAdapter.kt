package com.githubjavapop.activities.main_activity.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.githubjavapop.R
import com.githubjavapop.activities.main_activity.view_holder.LoadingViewHolder
import com.githubjavapop.model.Repository
import com.githubjavapop.activities.main_activity.view_holder.MainViewHolder


class MainAdapter(context: OnItemClickListener, val repositoryList: ArrayList<Repository>)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val ITEM = 0
    private val LOADING = 1
    private var isLoadingAdded = false

    private var mListener: OnItemClickListener? = null

    init {
        mListener = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        val viewHolder: RecyclerView.ViewHolder = when (viewType) {

            ITEM -> MainViewHolder((inflater).inflate(R.layout.row_repositories, parent, false))

            else -> LoadingViewHolder((inflater).inflate(R.layout.row_progress, parent, false))
        }
        return viewHolder

    }

    override fun getItemViewType(position: Int): Int {
        if (position == repositoryList.size - 1 && isLoadingAdded)
            return LOADING
        else
            return ITEM
    }

    override fun getItemCount(): Int {
        return repositoryList.size

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val repository = repositoryList.get(position)

        when (holder) {
            is MainViewHolder -> {
                holder.mTvUsername.setText(repository.owner?.login)
                holder.mTvRepositoryName.setText(repository.name)
                holder.mTvDescription.setText(repository.description)
                holder.mTvForksNumber.setText(repository.forks_count.toString())
                holder.mTvStarsNumber.setText(repository.stargazers_count.toString())
                holder.setImageUser(repository.owner?.avatar_url)

                holder.itemView.setOnClickListener { v ->
                    mListener?.onItemClick(repository.owner?.login, repository.name)
                }
            }
            is LoadingViewHolder -> holder.mProgress.setVisibility(VISIBLE)
        }

    }

    fun add(repository: Repository) {
        repositoryList.add(repository)
        notifyItemInserted(repositoryList.size - 1)

    }


    fun remove(repository: Repository) {
        val position = repositoryList.indexOf(repository)
        if (position > -1) {
            repositoryList.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun addLoadingFooter() {
        isLoadingAdded = true
        add(Repository("", "", 0, 0, null))
    }

    fun removeLoadingFooter() {
        isLoadingAdded = false
        val position = repositoryList.size - 1
        repositoryList.remove(getItem(position))
        notifyItemRemoved(position)
    }

    fun getItem(position: Int): Repository {
        return repositoryList.get(position)

    }

    interface OnItemClickListener {
        fun onItemClick(userId: String?, repositoryId: String)
    }


}