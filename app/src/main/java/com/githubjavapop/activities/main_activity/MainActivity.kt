package com.githubjavapop.activities.main_activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.githubjavapop.R
import com.githubjavapop.activities.main_activity.adapter.MainAdapter
import com.githubjavapop.model.Repository
import com.githubjavapop.api.GitHubAPI
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar_main_activity.*
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import com.githubjavapop.activities.pull_requests_activity.PullRequestsActivity
import com.githubjavapop.util.INTENT_REPOSITORY_ID
import com.githubjavapop.util.INTENT_USER_ID
import android.view.View.GONE
import android.support.v7.widget.RecyclerView




class MainActivity : AppCompatActivity(), MainAdapter.OnItemClickListener {

    private var repositoryList = ArrayList<Repository>()
    private var API: GitHubAPI? = null
    private var adapter: MainAdapter? = null


    //pagination
    private val PAGE_START = 1
    private var currentPage = PAGE_START

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()


        API = GitHubAPI()

        loadFirstPage()

    }



    private fun initViews(){

        //toolbar
        setSupportActionBar(toolbar_main)

        val layoutManager = LinearLayoutManager(this)
        //recycler view
        rv_repositories.setHasFixedSize(true)
        rv_repositories.layoutManager = layoutManager

        rv_repositories.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val reachedBottom = !recyclerView!!.canScrollVertically(1)

                if (reachedBottom) {
                    loadNextPage()
                }
            }
        })
    }


    private fun loadFirstPage(){

        API?.loadRepositories(currentPage)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    repository -> repositoryList.add(repository)

                }, {
                    e -> e.printStackTrace()
                }, {
                    displayRepositories(repositoryList)
                    currentPage++
                })

    }

    private fun loadNextPage(){

        API?.loadRepositories(currentPage)
                ?.subscribeOn(Schedulers.io())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribe({
                    repository -> repositoryList.add(repository)
                }, {
                    e -> e.printStackTrace()
                }, {
                    adapter?.removeLoadingFooter()

                })

    }

    private fun displayRepositories(repositories: ArrayList<Repository>){
        progress.setVisibility(GONE)
        adapter = MainAdapter(this, repositories)
        rv_repositories.adapter = adapter

        adapter?.addLoadingFooter()
    }



    override fun onItemClick(userId: String?, repositoryId: String) {
        startActivity(newIntent(userId, repositoryId))
    }

    fun newIntent(userId: String?, repositoryId: String): Intent {
        val intent = Intent(this, PullRequestsActivity::class.java)
        intent.putExtra(INTENT_USER_ID, userId)
        intent.putExtra(INTENT_REPOSITORY_ID, repositoryId)
        return intent
    }

}
