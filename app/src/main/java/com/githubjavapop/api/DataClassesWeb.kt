package com.githubjavapop.api

import com.githubjavapop.model.User
import com.google.gson.annotations.SerializedName


//REPOSITORIES
data class RepositoryResult(@SerializedName("items") val items: List<Items>)


data class Items(@SerializedName("name") val name: String,
                 @SerializedName("description") val description: String,
                 @SerializedName("forks_count") val forks_count: Int,
                 @SerializedName("stargazers_count") val stargazers_count: Int,
                 @SerializedName("owner") val owner: User)




data class Pulls(@SerializedName("user") val owner: User,
                @SerializedName("title") val title: String,
                @SerializedName("body") val body: String,
                @SerializedName("created_at") val created_at: String,
                @SerializedName("html_url") val html_url: String
)

