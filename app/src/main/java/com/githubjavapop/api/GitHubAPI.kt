package com.githubjavapop.api

import com.githubjavapop.model.PullRequest
import com.githubjavapop.model.Repository
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.Observable


class GitHubAPI{

    val service : GitHubApiDef

    init {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)

        val gson = GsonBuilder().setLenient().create()

        val retrofit = Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient.build())
                .build()

        service = retrofit.create<GitHubApiDef>(GitHubApiDef::class.java)
    }

    fun loadRepositories(page: Int) : Observable<Repository> {
        return service.getRepositories("language:Java", "stars", page)
                .flatMap { repositoryResult ->  Observable.from(repositoryResult.items)}
                .flatMap { repository -> Observable.just(Repository(repository.name, repository.description,
                        repository.forks_count,
                        repository.stargazers_count,
                        repository.owner))}
    }

    fun loadPullRequests(login: String?, name: String?) : Observable<PullRequest>{
        return service.getPullRequests(login, name)
                .flatMap { pulls ->  Observable.from(pulls)}
                .flatMap { pull -> Observable.just(PullRequest(pull.owner, pull.title, pull.body,
                        pull.created_at, pull.html_url)) }
    }
}