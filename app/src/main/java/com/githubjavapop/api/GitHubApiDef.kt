package com.githubjavapop.api

import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import rx.Observable

interface GitHubApiDef{

    @GET("search/repositories")
    fun getRepositories(@Query ("q") q : String, @Query("sort") sort : String,
                        @Query ("page") page : Int) : Observable<RepositoryResult>


    @GET("repos/{login}/{name}/pulls")
    fun getPullRequests(@Path("login") login: String?, @Path("name") name: String?) : Observable<List<Pulls>>
}